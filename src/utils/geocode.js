const request = require('request')

const geocode = (address, callback) => {
    const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + address + '.json?access_token=pk.eyJ1IjoicGVkcm9vbWVkaWNpbmEiLCJhIjoiY2s1dTFodzR6MTRvbzNrbjFxZnJjYzVxeCJ9.cBZNlObTh3CJYcrhqeUkKw'

    request({ url, json: true }, (error, { body }) => {
        if (error) {
            callback('Unable to connect to location services!', undefined)
        } else if (body.features && body.features.length === 0) {
            callback('Unable to find location. Try another search.', undefined)
        } else if (body.features){
            callback(undefined, {
                latitude: body.features[0].center[1],
                longitude: body.features[0].center[0],
                location: body.features[0].place_name
            })
        } else {
            callback(body.message, undefined);
        }
    })
}

module.exports = geocode